_model: question
---
title: How to circumvent the Great Firewall and connect to Tor from China?
---
seo_slug: connecting-from-china
---
key: 11
---
description:
Users in China need to take a few steps to circumvent the [Great Firewall](https://en.wikipedia.org/wiki/Great_Firewall) and connect to the Tor network.
First, get an updated version of Tor Browser: send an email to [gettor@torproject.org](mailto:gettor@torproject.org) with the subject "windows zh-cn" or other operating system (linux or macos).

After installing Tor Browser, you will probably not be able to connect directly to the Tor network, because the Great Firewall is blocking Tor.
Therefore, the second step will be to obtain a bridge that works in China.

There are three options to unblock Tor in China:

1. **[Snowflake](../what-is-snowflake/):** uses ephemeral proxies to connect to the Tor network.
It's available in Tor Browser.
You can select Snowflake from Tor Browser's [built-in bridge menu](../how-can-i-use-snowflake/).
1. **Private and unlisted obfs4 bridges:** contact our Telegram Bot [@GetBridgesBot](https://t.me/GetBridgesBot) and type `/bridges`.
Or send an email to [frontdesk@torproject.org](mailto:frontdesk@torproject.org) with the phrase "private bridge" in the subject of the email.
If you are tech-savvy, you can run your own [obfs4 bridge](https://community.torproject.org/relay/setup/bridge/) from outside China.
Remember that bridges distributed by [BridgeDB](https://bridges.torproject.org), and built-in obfs4 bridges bundled in Tor Browser most likely won't work.
1. **meek-azure:** makes it look like you are browsing a Microsoft website instead of using Tor.
However, because it has a bandwidth limitation, this option will be quite slow.
You can select meek-azure from Tor Browser's built-in bridges dropdown.

If one of these options above is not working, check your [Tor logs](../../connecting/connecting-2/) and try another option.
